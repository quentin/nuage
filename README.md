## nuage

`nuage` is a tool to deploy instances on scaleway.
You can see it as a basic re-implementation of a tool like Terraform, specially tailored for my needs/workflow.

you will need Scaleway's command line tool to setup your tokens.
  - [install it](https://github.com/scaleway/scaleway-cli#installation)
  - run `scw init`

nuage is idempotent, so if you run twice the same command, it will not fail but just say that the operation has already be done.
nuage, and not Scaleway, requires that instance's name are uniques to provide this idempotency feature.

## install

  1. Install go on your machine (cf golang.org)
  2. Clone this repository
  3. Run `go build`

## usage

Now, you can create your own inventory, the file format is basic: `<zone> <instance model> <image> <name>`.
See the committed `inventory.txt` for an example.

Then it could be used as follow:

```
./nuage spawn < inventory.txt
./nuage run <(echo "touch /etc/nuage.txt") < inventory.txt
./nuage destroy < inventory.txt
```

  1. It will spawn the 2 instances defined in the inventory
  2. It will run `touch ...` on all instances of the inventory
  3. It will power off then delete the instances



