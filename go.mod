module git.deuxfleurs.fr/quentin/nuage

go 1.16

require (
	github.com/pkg/sftp v1.13.3
	github.com/scaleway/scaleway-sdk-go v1.0.0-beta.7
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)
